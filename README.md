# SXHKD-MODAL
wip scripts to use sxhkd in a modal interface

## usage
put global bindings (all modes) in $SXHKD_DIR/sxhkdrc

put modes in $SXHKD_DIR/modes/$MODE_NAME

the default mode is normal
```
    -d, --daemon                         start daemon in foreground
    -k, --kill                           kill a running daemon
    -c, --config: <PATH>                 select config dir (default $XDG_CONFIG_HOME/sxhkd)
    -m, --mode:   <MODE[,MODE2,..]>      select mode[s]
    -h, --help                           show help text
    -V, --version                        show version
```
